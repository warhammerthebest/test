package com.noir.myapplication.api

import com.noir.myapplication.models.Weather
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface Api {
    @Headers("X-Yandex-API-Key: dcb69791-d6e0-486b-99de-175131a2c495")
    @GET("forecast")
    fun getWeather(@Query("lat") lat: Double,
                   @Query("lon") lon: Double,
                   @Query("extra") extra: Boolean = true,
                   @Query("lang") lang: String = "ru_RU",
                   @Query("limit") limit: Int = 1): Observable<Weather>;
}