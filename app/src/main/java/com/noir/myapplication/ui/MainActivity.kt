package com.noir.myapplication.ui

import android.location.Address
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.noir.myapplication.DaggerAppComponent
import com.noir.myapplication.NetworkModule
import com.noir.myapplication.R
import com.noir.myapplication.api.Api
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var api: Api

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        injectDependency()
        val compositeDisposable = CompositeDisposable()
        button.setOnClickListener {
            textView.text = ""
            val addresses: List<Address>
            val geocoder = Geocoder(this)
            if (editText.text.isNotEmpty()) {
                addresses = geocoder.getFromLocationName(editText.text.toString(), 1)
                if (addresses.isNotEmpty()) {
                    compositeDisposable.add(api.getWeather(
                        addresses[0].latitude,
                        addresses[0].longitude
                    )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ weather ->
                            textView.text = getString(R.string.weather,
                                weather.fact.temp.toString(),
                                weather.fact.feels_like.toString(),
                                weather.fact.wind_speed.toString(),
                                weather.fact.condition)
                        }, { error ->
                            error.stackTrace
                        })
                    )
                } else {
                    Toast.makeText(this, getString(R.string.address_not_found), Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, getString(R.string.address_is_empty), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun injectDependency() {
        val appComponent = DaggerAppComponent.builder().networkModule(NetworkModule()).build()
        appComponent.inject(this)
    }
}