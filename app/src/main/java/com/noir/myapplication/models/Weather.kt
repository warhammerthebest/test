package com.noir.myapplication.models

data class Weather(val now: Long, val now_dt: String, val fact: Fact)
data class Fact(val temp: Int, val feels_like: Int, val condition: String, val wind_speed: Double)